#!/usr/bin/python

__author__ = 'steve'

from lib.geo_objects import *
from lib.geo_methods import *

if __name__ == "__main__":
    # test of afrl data

    points = Points('/home/steve/GISData/afrl_mine_portal/test_data/random_points.shp', 'ID')

    rl = raster_library_from_list_file('/media/sf_E_DRIVE/raster_list.txt')
    rl.set_proj_epsg(3735)

    def mean(x):
        if not x:
            return None
        a = [a for a in x if a > 0]
        if len(a) > 0:
            return sum(a) / len(a)
        else:
            return None

    print {
    i: [rl.sample_coord(point_to_raster(points, rl, p), b, 10, lambda x: sum(x) / len(x), mean) for b in rl.bands] for
    i, p in points}