#!/usr/bin/python
# __author__ = 'steve'
import argparse
import csv
from lib.geo_objects import *
from lib.geo_methods import *


def mean(x):
    if not x:
        return None
    a = [a for a in x if a > 0]
    if len(a) > 0:
        return sum(a) / len(a)
    else:
        return None

if __name__ == "__main__":

    # create an argparse and set up command line arguments
    parser = argparse.ArgumentParser(description="Samples raster(s) for pixel values at each point in point gis data")
    parser.add_argument('-p', '--points', type=str, help='point data to sample raster(s) for', required=True)
    parser.add_argument('-i', '--id_field', type=str, help='field of point data to use as unique identifier'
                        , required=True)
    parser.add_argument('-r', '--raster', type=str, help='raster(s) to use for sampling', nargs='*')
    parser.add_argument('-rl', '--list', type=str, help='file list of raster(s) to use for sampling')
    parser.add_argument('-rp', '--rast_epsg', type=int, help='EPSG code of raster projection to enforce')
    parser.add_argument('-b', '--band', default=0, type=int, help='band number to sample, 0 means all bands')
    parser.add_argument('-w', '--window', default=1, type=int, help='size of window around pixels to sample')
    parser.add_argument('-o', '--outfile', default='outfile.csv', type=str, help='Path of output csv file to write')
    parser.add_argument('--stdout', action='store_true', help='Print results to stdout')
    args = parser.parse_args()

    #checking to see if we should use raster library or just raster
    if args.list:
        rl = raster_library_from_list_file(args.list)
    elif len(args.raster) > 1:
        rl = RasterLibrary(args.raster)
    else:
        rl = Raster(args.raster[0])

    if args.rast_epsg:
        rl.set_proj_epsg(args.rast_epsg)

    #opening points
    p = Points(args.points, args.id_field)

    #if args.band is zero we use all bands
    if args.band:
        bands = [args.band]
    else:
        bands = rl.bands

    #fetch pixel values for each coord in points
    header = [args.id_field] + bands
    if not args.stdout:
        csv_w = csv.DictWriter(open(args.outfile, 'w'), fieldnames=header)
        csv_w.writeheader()
    for i, point in p:
        data = {args.id_field: i}
        data.update({b: rl.sample_coord(point_to_raster(p, rl, point), b, window_size=args.window,
                                        window_function=lambda x: sum(x) / len(x), overlap_function=mean) for b in
                     bands})
        if args.stdout:
            print data
        else:
            csv_w.writerow(data)