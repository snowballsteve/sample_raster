#Sample Raster

Sample raster is a tool to read the pixel values of one or multiple overlapping rasters using a shapefile of point data. The tool generates a csv file or can dump to stdout the results.

Contact Steven Porter snowballsteve3@gmail.com for more information.