__author__ = 'steve'

from osgeo import gdal, osr, ogr
import struct
import os
import numpy

class PointError(Exception):
    pass


class RasterError(Exception):
    pass


class RasterLibraryError(Exception):
    pass


class Points:
    """
    Point geometry handling. Used as generator of (id_field,(x,y)) tuples
    """

    def __init__(self, file_path, id_field):
        """

        :param file_path: path to ogr point file
        :return: None
        """
        self.id = id_field
        self.data = ogr.Open(file_path)
        self.layer = self.data.GetLayer()
        if self.layer.GetGeomType() != 1:
            raise PointError('Geometry type is not wkbPoint')
        if id_field not in self.__get_fields():
            raise PointError('Field (%s) not found in point layer' % id_field)
        self.proj = self.layer.GetSpatialRef()

    def __get_fields(self):
        return [self.layer.GetLayerDefn().GetFieldDefn(i).GetName() for i in
                range(self.layer.GetLayerDefn().GetFieldCount())]

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self):
        """
        :return: tuple (X,Y) representing point coordinates
        """
        feat = self.layer.next()
        geom = feat.GetGeometryRef()
        return (feat.GetField(self.id), (geom.GetX(), geom.GetY()))


class Raster:
    """ handles reading of pixel values from a gdal raster, currently assumes raster is of unsigned int """

    def __init__(self, file_path):
        """

        :param file_path: path to raster image
        :return: None
        """
        if not os.path.exists(file_path):
            raise RasterError("File path does not exist \n%s" % file_path)
        self.path = file_path
        self.raster = gdal.Open(self.path)
        self.gt = self.raster.GetGeoTransform()
        self.proj = osr.SpatialReference()
        self.proj.ImportFromWkt(self.raster.GetProjection())
        minx = self.gt[0]
        miny = self.gt[3]
        maxx = self.gt[0] + self.gt[1] * self.raster.RasterXSize
        maxy = self.gt[3] + self.gt[5] * self.raster.RasterYSize
        if minx > maxx:
            minx, maxx = maxx, minx
        if miny > maxy:
            miny, maxy = maxy, miny
        self.extent = (minx, miny, maxx, maxy)
        self.bands = range(1, self.raster.RasterCount + 1)

    def set_proj_epsg(self, epsg):
        """
        Sets the raster projection to the epsg code
        :param epsg: EPSG code (int) to use
        :return: None
        """
        new_proj = osr.SpatialReference()
        new_proj.ImportFromEPSG(epsg)
        self.proj = new_proj

    def __get_band(self, n):
        """

        :param n: band number of raster to retreive
        :return: raster band object
        """
        if n not in self.bands:
            raise RasterError("Invalid band %d" % n)
        return self.raster.GetRasterBand(n)

    def sample_coord(self, coord, band=1, window_size=1, window_function=lambda x: x):
        """
        Samples a raster band and returns an array of pixel coordinates from a square window around a coordinate

        :param coord: tuple (x,y) in map coordinates
        :param window_size: size of window
        :param band: band object to read pixels of
        :return: list(int) of pixel values
        """
        pixel_x, pixel_y = self.map_to_pixel(coord)
        raster_band = self.__get_band(band)
        if pixel_x > self.raster.RasterXSize:
            raise RasterError("Pixel X value is larger than raster")
        if pixel_y > self.raster.RasterYSize:
            raise RasterError("Pixel Y value is larger than raster")

        # get struct value
        sv = raster_band.ReadRaster(pixel_x, pixel_y, window_size, window_size, buf_type=gdal.GDT_UInt16)
        # convert struct to integers
        iv = struct.unpack('h' * window_size * window_size, sv)

        if len(iv) == 1:
            return iv[0]
        else:
            return window_function(iv)

    def sample_coords(self, coords, band=1, window_size=1, window_function=lambda x: x):
        """
        Returns a list of list(int) of pixel values for the window around each coordinate

        :param coords:  list(tuple (x,y)) in pixel coordinates
        :param window_size: size of window
        :param band: band object to read pixels of
        :return: list(list(int)) of pixel values
        """
        return [self.sample_coord(c, window_size, band, window_function) for c in coords]

    def coord_in_raster(self, coord):
        """
        Returns true if coord is in raster extent. Coord assumed to be in raster coordinate system
        :param coord: tuple (x,y) for xy coordinate in raster coordinate system
        :param raster: raster to check extent of
        :return: boolean True if coord within raster extent
        """
        x, y = coord
        xmin, ymin, xmax, ymax = self.extent
        return xmin <= x <= xmax and ymin <= y <= ymax

    def map_to_pixel(self, coord):
        """
        Returns pixel coordinates from raster map coordinates

        :param coord:
        :param raster:
        :return:
        """
        x, y = coord
        px = int((x - self.gt[0]) / self.gt[1])
        py = int((y - self.gt[3]) / self.gt[5])
        return px, py

    def latlng_to_map(self, latlng):
        """
        Returns raster coordinates from latitude and longitude coordinates

        :param latlng: tuple (lon,lat) representing latitude and longitude coordinate
        :param raster: raster object to convert lat long to to raster (x,y)
        :return: tuple (x,y) representing raster coordinate system
        """
        lon, lat = latlng
        wkt = 'POINT(%f %f)' % (lon, lat)
        wgs84 = osr.SpatialReference()
        wgs84.ImportFromEPSG(4326)
        point = ogr.CreateGeometryFromWkt(wkt)
        point.AssignSpatialReference(wgs84)
        point.TransformTo(self.proj)
        return point.GetX(), point.GetY()

    def latlng_to_pixel(self, latlng):
        """
        Returns pixel coordinates from latitude and longitude coordinates

        :param latlng: tuple (lon,lat) representing latitude and longitude coordinate
        :return: tuple (x,y) representing raster pixel coordinates
        """
        map_xy = self.latlng_to_map(latlng)
        return self.map_to_pixel(map_xy)


class RasterLibrary:
    def __init__(self, list_of_files):
        """
        Initialized with stored array of raster objects from file paths. All rasters must have same coordinate system and number of bands.
        :param list_of_files: list of file paths
        :return: None
        """
        self.rasters = [Raster(file_path) for file_path in list_of_files]
        self.paths = [r.path for r in self.rasters]
        self.extent = (min([r.extent[0] for r in self.rasters]), min([r.extent[1] for r in self.rasters]),
                       max([r.extent[2] for r in self.rasters]), max([r.extent[3] for r in self.rasters]))
        if len(set([r.proj for r in self.rasters])) > 1:
            RasterLibraryError("Not all rasters have the same coordinate system.")
        self.proj = self.rasters[0].proj
        if len(set([len(r.bands) for r in self.rasters])) > 1:
            RasterLibraryError("Not all rasters have the same number of bands.")
        self.bands = self.rasters[0].bands

    def set_proj_epsg(self, epsg):
        for r in self.rasters:
            r.set_proj_epsg(epsg)
            self.proj = self.rasters[0].proj

    def get_rasters_by_coord(self, coord):
        """
        Returns list of raster objects for all rasters that contain coordinate. Coord must be in raster projection
        :param coord: tuple (x,y)
        :return: list(Raster)
        """
        return [r for r in self.rasters if r.coord_in_raster(coord)]


    def includes_coord(self, coord):
        """
        Return true if coord is in any raster in library

        :param coord: (x,y) coordinate in raster coordinate system
        :return:
        """
        return any([r.coord_in_raster(coord) for r in self.rasters])


    def sample_coord(self, coord, band=1, window_size=1, window_function=lambda x: x, overlap_function=numpy.mean):
        """
        Returns a list of pixel values for each intersected raster

        :param coord: tuple (x,y) in map coordinates
        :param window_size: size of window
        :param band: band object to read pixels of
        :param function: function used to reduce pixels for multiple raster intersections. defaults to mean
        :return: list(list(int)) of pixel values, one sublist per raster
        """
        results = [rast.sample_coord(coord, band, window_size, window_function) for rast in
                   self.get_rasters_by_coord(coord)]
        if not len(results): return None
        if type(results[0]) == int:
            return overlap_function(results)
        else:
            return [[overlap_function(r) for r in sub_r] for sub_r in results]


    def sample_coords(self, coords, band=1, window_size=1, window_function=lambda x: x, overlap_function=numpy.mean):
        """
        Returns a list of lists of lists, each sublist containing pixel values for each coordinate each subsublist for each coordinate and raster intersected

        :param coord: tuple (x,y) in map coordinates
        :param window_size: size of window
        :param band: band object to read pixels of
        :param function: function used to reduce pixels for multiple raster intersections. defaults to mean
        :return: list(list(list(int))) of pixel values, one sublist per raster
        """
        return [self.sample_coord(c, window_size, band, window_function, overlap_function) for c in coords]
