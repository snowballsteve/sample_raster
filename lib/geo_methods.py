__author__ = 'steve'

from osgeo import osr, ogr
from lib.geo_objects import RasterLibrary
import os


def point_to_raster(point, raster, coord):
    """
    Transform coord from point coordinate system to  raster coordinate system

    :param point: point object
    :param raster: raster object
    :param coord: tuple (x,y) representing coordinate in point object system
    :return: tuple (x,y) of coordinate in raster object system
    """
    x, y = coord
    wkt = 'POINT(%f %f)' % (x, y)
    p = ogr.CreateGeometryFromWkt(wkt)
    p.AssignSpatialReference(point.proj)
    p.TransformTo(raster.proj)
    return p.GetX(), p.GetY()


def raster_library_from_list_file(list_file):
    """
    Returns a RasterLibrary from a list of raster file paths stored in a text file (aka output from ls)
    :param list_file: Text file with file name on each line
    :return: RasterLibrary
    """
    path = list(os.path.split(list_file))[0]
    r_paths = [os.path.join(path, r.strip()) for r in open(list_file, 'r')]
    return RasterLibrary(r_paths)

